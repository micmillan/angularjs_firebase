"use strict";

var gulp = require("gulp"),
    sass = require("gulp-sass"),
    cssmin = require("gulp-cssmin"),
    rename = require("gulp-rename"),
    livereload = require("gulp-livereload"),
    webserver = require("gulp-webserver");

gulp.task("update-html", function(){
  return gulp.src("app/**/*.html")
    .pipe(livereload());
});

gulp.task("build-sass", function(){
  return gulp.src("app/scss/styles.scss")
    .pipe(sass())
    .pipe(cssmin())
    .pipe(rename({suffix: ".min"}))
    .pipe(gulp.dest("app/css"))
    .pipe(livereload());
});

gulp.task("webserver", function() {
  gulp.src("app")
    .pipe(webserver({
      host: "localhost",
      port: "9999",
      livereload: true,
      open: true
    }));
});

gulp.task("watch", function(){
  livereload.listen();
  gulp.watch("app/**/*.scss", ["update-html"]);
  gulp.watch("app/scss/**/*.scss", ["build-sass"]);
});

gulp.task("default", [
  "update-html",
  "build-sass",
  "watch",
  "webserver"
]);
